/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author ASUS
 */
public class Player {
    private char Symbol;
    private int winCount,loseCount,drawCount;

    public Player(char Symbol) {
        this.Symbol = Symbol;
        this.winCount = 0;
        this.loseCount = 0;
        this.drawCount = 0;
    }
    public void win(){
        winCount++;
    }
    public void lose(){
        loseCount++;
    }
    public void draw(){
        drawCount++;
    }
    public char getSymbol() {
        return Symbol;
    }

    public int getWinCount() {
        return winCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public int getDarwCount() {
        return drawCount;
    }

    @Override
    public String toString() {
        return "Player{" + "Symbol=" + Symbol + ", winCount=" + winCount + ", loseCount=" + loseCount + ", drawCount=" + drawCount + '}';
    }
    
    
}
