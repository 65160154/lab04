/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player player1;
    private Player player2;
    private Player CP;
    private int turnCount = 0;
    private int row, col;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.CP = player1;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCP() {
        return CP;
    }

    public boolean setRowcol(int row, int col) {
        if (table[row - 1][col - 1] == '-') {
            table[row - 1][col - 1] = CP.getSymbol();
            this.row = row;
            this.col = col;
            return true;
        }
        return false;

    }

    public void switchPlayer() {
        turnCount++;
        if (CP == player1) {
            CP = player2;
        } else {
            CP = player1;
        }
    }

    public boolean checkWin() {
        if (checkRow()) {
            savewin();
            return true;
        }
        if (checkCol()) {
            savewin();
            return true;

        }
        if (checkDL()) {
            savewin();
            return true;
        }
        if (checkDR()) {
            savewin();
            return true;
        }
        if (checkDraw()) {
            savewin();
            return true;
        }
        return false;
    }

    private boolean checkRow() {
        char symbol = CP.getSymbol();
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != symbol) {
                return false;
            }
        }
        return true;

    }

    private boolean checkCol() {
        char symbol = CP.getSymbol();
        for (int j = 0; j < 3; j++) {
            if (table[j][col - 1] != symbol) {
                return false;
            }
        }
        return true;

    }

    private boolean checkDR() {
        char symbol = CP.getSymbol();
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != symbol) {
                return false;
            }
        }
        return true;
    }

    private boolean checkDL() {
        char symbol = CP.getSymbol();
        for (int i = 0; i < 3; i++) {
            if (table[i][2 - i] != symbol) {
                return false;
            }
        }
        return true;
    }

    boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;

    }

    private void savewin() {
        if (player1 == getCP()) {
            player1.win();
            player2.lose();
        } else {
            player2.win();
            player1.lose();
        }
    }

    private void saveDraw() {
        player1.draw();
        player2.draw();

    }
    
    private void Restart(){
        System.out.println("Restart(Y/N ?)");
        Scanner SC = new Scanner(System.in);
        String P = SC.next();
        if(P.equals("Y")){
             for(int i = 0;i<3;i++){
                for(int j =0;j<3;j++){
                    table[i][j] = '-';

        }
    }

}
    }
}
